#!/usr/bin/env python

from setuptools import setup, find_packages


setup(
    name="django-rpc-crud",
    version="0.1",
    packages=find_packages(),
    zip_safe=True,

    install_requires=[
        "django>=1.6",
        "rpc4django>=0.2.5",
    ],

    author="Sergey Blinov",
    author_email="blinovsv@gmail.com",
    url="",
    description="",
    long_description="",
)
