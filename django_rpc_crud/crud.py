from rpc4django import rpcmethod


class ModelCrud(object):
    methods = [
        'get',
        'create',
        'update',
        'delete',
    ]

    def __init__(self, model):
        self.model = model
        self.app_name = model._meta.app_label
        self.model_name = model._meta.model_name

    def get(self, ids):
        return list(self.model.objects.filter(id__in=ids).values())

    def create(self, data):
        obj = self.model(**data)
        obj.save()
        return list(self.model.objects.filter(id=obj.id).values())

    def update(self, objects):
        for data in objects:
            if 'id' in data:
                self.model.objects.filter(id=data['id']).update(**data)
        return True

    def delete(self, pks):
        self.model.objects.filter(pk__in=pks).delete()
        return True

    def _permission(self, permission):
        return '{}.{}_{}'.format(self.app_name, permission, self.model_name)

    def _register_method(self, method):
        unique_method_name = "{app}.{model}.{method}".format(
            app=self.app_name,
            model=self.model_name,
            method=method
        )

        rpcmethod_kwargs = dict(
            name=unique_method_name,
        )

        if method is ['create']:
            rpcmethod_kwargs['permission'] = self._permission('add')
        elif method is ['delete']:
            rpcmethod_kwargs['permission'] = self._permission('delete')
        else:
            rpcmethod_kwargs['permission'] = self._permission('change')

        crud_fn = lambda *args, **kwargs: getattr(self, method)(*args, **kwargs)

        # XXX: register function in global, because rpc4django finds
        # functions in global namespaces, instead of save links
        globals()[unique_method_name] = crud_fn
        rpcmethod(**rpcmethod_kwargs)(crud_fn)

    def register(self):
        for method in self.methods:
            self._register_method(method)


def register(model, model_crud=None):
    model_crud = model_crud or ModelCrud
    crud = model_crud(model)
    crud.register()
