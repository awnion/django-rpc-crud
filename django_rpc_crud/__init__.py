import importlib

from django.conf import settings


for app in settings.INSTALLED_APPS:
    try:
        importlib.import_module('.'.join([app, 'rpc']))
    except ImportError:
        pass
